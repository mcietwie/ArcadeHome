package com.example.martin.mamehome;

/**
 * Created by Martin on 9/27/2015.
 */
public class Game {
    public Console console;
    public String name;
    public String romName;

    public Game( Console console, String name, String romName ) {
        this.console = console;
        this.name = name.replace(" ", "   ");;
        this.romName = romName;
    }
}
