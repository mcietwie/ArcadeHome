package com.example.martin.mamehome;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.MotionEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Set;

/**
 * Created by Martin on 9/29/2015.
 */
public class ViewDebug extends ViewArcade {
    private final String LOG_TAG = "ARCADE_HOME";

    public Home home;

    private final String invalidAddr = "x,x,x,x";
    private boolean wifiOn = false;
    private String wifiAddr = invalidAddr;
    private String wifiSSID = "none";
    private boolean bluetoothOn = false;
    private Set<BluetoothDevice> btDevices = null;
    private Rect adbButtonRect = new Rect( 0, 0, 0, 0);
    private Rect wifiButtonRect = new Rect( 0, 0, 0, 0);

    private boolean adbWifi = false;

    private Process suProcess;

    private long ticks = 0;

    private int lastKeyDown = 0;
    private int lastKeyUp = 0;

    public ViewDebug(Home home) {
        super( home );
        this.home = home;
        setupConsoles();
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    private void setupConsoles() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
    }

    @Override
    protected void update( float timeDelta) {
        if ( ticks % 30 != 0 ) {
            ticks++;
            return;
        }
        ticks++;

        wifiAddr = invalidAddr;
        wifiSSID = "none";

        WifiManager wifi = (WifiManager)home.getSystemService(Context.WIFI_SERVICE);
        if ( wifi != null ) {
            wifiOn = wifi.isWifiEnabled();
            WifiInfo winfo = wifi.getConnectionInfo();
            if ( winfo != null ) {
                wifiSSID = winfo.getSSID();
                if ( wifiSSID == null ) wifiSSID = "none";
                if ( winfo.getIpAddress() == 0 ) {
                    wifiAddr = invalidAddr;
                } else {
                    wifiAddr = Formatter.formatIpAddress(winfo.getIpAddress());
                    wifiAddr = wifiAddr.replace('.', ',');
                }
            }
        } else {
            wifiOn = false;
        }

        btDevices = null;
        BluetoothAdapter bt = BluetoothAdapter.getDefaultAdapter();
        if ( bt != null ) {
            bluetoothOn = bt.isEnabled();
            btDevices = bt.getBondedDevices();
        } else {
            bluetoothOn = false;
        }

        try {
            suProcess = Runtime.getRuntime().exec("su -c getprop service.adb.tcp.port");
        } catch ( IOException e ) {
            Log.e(LOG_TAG, "Failed to run adb commands " + e.toString());
            return;
        }
        if ( suProcess != null ) {
            InputStream stdin = suProcess.getInputStream();
            InputStreamReader isr = new InputStreamReader(stdin);
            BufferedReader br = new BufferedReader(isr);
            String line = "-1";
            try {
                line = br.readLine();
            } catch ( IOException e ) {
            }

            Integer port = -1;
            if ( line == null || line.equals("") ) line = "-1";
            port = Integer.parseInt( line );

            if ( port == -1 ) {
                adbWifi = false;
            } else {
                adbWifi = true;
            }
        }
    }

    private void toggleAdb() {
        if ( !adbWifi ) {
            try {
                suProcess = Runtime.getRuntime().exec("su -c setprop service.adb.tcp.port 5555");
                suProcess = Runtime.getRuntime().exec("su -c stop adbd");
                suProcess = Runtime.getRuntime().exec("su -c start adbd");
            } catch ( IOException e ) {
                Log.e(LOG_TAG, "Failed to run adb commands " + e.toString());
                return;
            }
        }
        else {
            try {
                suProcess = Runtime.getRuntime().exec("su -c setprop service.adb.tcp.port -1");
                suProcess = Runtime.getRuntime().exec("su -c stop adbd");
                suProcess = Runtime.getRuntime().exec("su -c start adbd");
            } catch ( IOException e ) {
                Log.e(LOG_TAG, "Failed to run adb commands " + e.toString());
                return;
            }
        }
    }

    private void toggleWifi() {
        WifiManager wifi = (WifiManager)home.getSystemService(Context.WIFI_SERVICE);
        if ( wifi != null ) {
            if (wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLED || wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
                wifi.setWifiEnabled( false );
            } else {
                wifi.setWifiEnabled( true );
            }
        }
    }

    @Override
    protected void drawScene( Canvas canvas ) {
        Util.paintShape.setARGB(255, 0, 0, 16);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), Util.paintShape);

        int yOffset = 10;
        int xOffset = 10;
        int ySpacing = 10;

        int keys[] = { Util.PTKEY_UP, Util.PTKEY_DOWN, Util.PTKEY_LEFT, Util.PTKEY_RIGHT,
                       Util.PTKEY_A, Util.PTKEY_B, Util.PTKEY_X, Util.PTKEY_Y,
                       Util.PTKEY_LEFT_SHOULDER, Util.PTKEY_RIGHT_SHOULDER,
                       Util.PTKEY_START, Util.PTKEY_SELECT,
                       Util.PTKEY_PAUSE, Util.PTKEY_EXIT,
                       Util.PTKEY_VOL_UP, Util.PTKEY_VOL_DOWN};

        String keyNames[] = { "Up", "Down", "Left", "Right",
                              "A", "B", "X", "Y",
                              "Left Shoulder", "Right Shoulder",
                              "Start", "Select",
                              "Pause", "Exit",
                              "Vol+", "Vol-"};

        for ( int i = 0; i < keys.length; i++ ) {
            if ( Home.keyStates[keys[i]]) Util.paintSmwGameFont.setARGB(255, 64, 255, 64);
            else Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
            canvas.drawText(keyNames[i], xOffset, yOffset, Util.paintSmwGameFont);
            yOffset += ySpacing;
        }
        yOffset += ySpacing;
        if ( lastKeyDown != 0 ) {
            canvas.drawText("Key Down:  " + lastKeyDown, xOffset, yOffset, Util.paintSmwGameFont);
        } else if (lastKeyUp != 0) {
            canvas.drawText("Key Up:  " + lastKeyUp, xOffset, yOffset, Util.paintSmwGameFont);
        }

        yOffset = 10;
        xOffset = 120;
        int colSpacing = 120;

        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("Wifi On: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        canvas.drawText(wifiOn ? "Yes" : "No", xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        yOffset += ySpacing;

        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("Wifi SSID: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        canvas.drawText(wifiSSID, xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        yOffset += ySpacing;

        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("Wifi IP: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        canvas.drawText(wifiAddr, xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        yOffset += ySpacing;
        yOffset += ySpacing;

        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("Bluetooth On: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        canvas.drawText(bluetoothOn ? "Yes" : "No", xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        yOffset += ySpacing;

        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("Bluetooth Devices: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);

        if ( btDevices == null ) {
            canvas.drawText("none", xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        } else {
            for (BluetoothDevice btDevice : btDevices) {
                yOffset += ySpacing;
                canvas.drawText(btDevice.getName(), xOffset + 30, yOffset, Util.paintSmwGameFont);
            }
        }
        yOffset += ySpacing;
        yOffset += ySpacing;


        Util.paintSmwGameFont.setARGB(255, 96, 96, 96);
        canvas.drawText("ADB mode: ", xOffset, yOffset, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        canvas.drawText(adbWifi ? "Wifi" : "USB", xOffset + colSpacing, yOffset, Util.paintSmwGameFont);
        yOffset += ySpacing;
        yOffset += ySpacing;

        wifiButtonRect = new Rect( xOffset, yOffset, xOffset + 170, yOffset + 40 );

        Util.paintShape.setARGB(255, 64, 64, 64);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);

        canvas.drawRect(wifiButtonRect, Util.paintShape);
        canvas.drawText("Wifi", xOffset + 20, yOffset + 15, Util.paintSmwGameFont);
        canvas.drawText("Touch or LS+RS+Start", xOffset + 20, yOffset + 30, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        yOffset += 40 + ySpacing;
        yOffset += ySpacing;

        adbButtonRect = new Rect( xOffset, yOffset, xOffset + 170, yOffset + 40 );

        Util.paintShape.setARGB(255, 64, 64, 64);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);

        canvas.drawRect(adbButtonRect, Util.paintShape);
        canvas.drawText("Toggle ADB Mode", xOffset + 20, yOffset + 15, Util.paintSmwGameFont);
        canvas.drawText("Touch or LS+RS+Select", xOffset + 20, yOffset + 30, Util.paintSmwGameFont);
        Util.paintSmwGameFont.setARGB(255, 255, 255, 255);
        yOffset += 40 + ySpacing;
        yOffset += ySpacing;

        xOffset = 10;
        Util.paintSmwGameFont.setARGB(255, 64, 64, 64);
        if ( adbWifi && !wifiAddr.equals(invalidAddr )) {
            canvas.drawText("To adb over wifi type: adb connect " + wifiAddr + ": 5555", xOffset, yOffset, Util.paintSmwGameFont);
        } else {
            canvas.drawText("To adb over wifi toggle on wifi connection and adb wifi mode", xOffset, yOffset, Util.paintSmwGameFont);
        }
        yOffset += ySpacing;
    }

    @Override
    public void handleKey( int keyCode, boolean down ) {
        if ( down ) {
            if (Home.keyStates[Util.PTKEY_LEFT_SHOULDER] && Home.keyStates[Util.PTKEY_RIGHT_SHOULDER]) {
                if (keyCode == Util.PTKEY_START) {
                    toggleWifi();
                } else if (keyCode == Util.PTKEY_SELECT) {
                    toggleAdb();
                }
            }
        }

        if ( down ) {
            lastKeyDown = keyCode;
            lastKeyUp = 0;
        } else {
            lastKeyUp = keyCode;
            lastKeyDown = 0;
        }
    }

    public boolean handleTouch(MotionEvent event) {
        switch ( event.getAction() ) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                if ( adbButtonRect.contains((int) event.getX()/3, (int) event.getY()/3)) {
                    toggleAdb();
                }
                else if ( wifiButtonRect.contains((int) event.getX()/3, (int) event.getY()/3)) {
                    toggleWifi();
                }
                break;

        }
        return true;
    }
}
