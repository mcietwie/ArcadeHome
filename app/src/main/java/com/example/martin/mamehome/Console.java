package com.example.martin.mamehome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 9/26/2015.
 */
public class Console {
    int frame;
    String name;
    Bitmap bitmap;
    public String emulatorName;
    public String romPath;
    private ViewConsole viewConsole;

    List<Game> games = new ArrayList<Game>();

    int currentSelection = 0;
    float scrollY = 0;
    int lineSpacing = 12;

    public Console(Bitmap srcBitmap, int srcFrame, ViewConsole viewConsole, String srcName, String emulatorName, String romPath ) {
        bitmap = srcBitmap;
        frame = srcFrame;
        name = srcName.replace(" ", "   ");
        this.emulatorName = emulatorName;
        this.romPath = romPath;
        this.viewConsole = viewConsole;
    }

    protected void update( float timeDelta) {
        int destY = currentSelection * -lineSpacing;

        float delta = (destY - scrollY) / 4 * timeDelta * 20.0f;
        if ( delta < 0 && delta > -1.0f ) delta = -1.0f;
        if ( delta > 0 && delta < 1.0f ) delta = 1.0f;

        scrollY += delta;
        if ( Math.abs(destY - scrollY) < 1.1f ) scrollY = destY;
    }

    protected void draw( Canvas canvas, boolean selected ) {
        Util.drawBitmapFrame( canvas, bitmap, 128, frame, canvas.getWidth() / 2 - 128 / 2, canvas.getHeight() / 2 - bitmap.getHeight() / 2 - 20 );

        Rect textBounds = new Rect();
        Util.paintSmwFont.getTextBounds(name, 0, name.length(), textBounds);
        Util.paintSmwFont.measureText(name);
        canvas.drawText(name, canvas.getWidth() / 2 - textBounds.width() / 2, canvas.getHeight() / 2 + bitmap.getHeight() / 2, Util.paintSmwFont);

        if ( selected ) {
            int i = 0;
            for ( Game game : games ) {
                textBounds = new Rect();
                Util.paintSmwGameFont.getTextBounds(game.name, 0, game.name.length(), textBounds);
                Util.paintSmwGameFont.measureText(game.name);
                if ( i != currentSelection ) {
                    Util.paintSmwGameFont.setARGB( Util.paintSmwGameFont.getAlpha(), 96, 96, 96 );
                } else {
                    Util.paintSmwGameFont.setARGB( Util.paintSmwGameFont.getAlpha(), 255, 255, 255 );
                }
                canvas.drawText(game.name, canvas.getWidth() / 2 - 128 / 2 + 150, canvas.getHeight() / 2 - textBounds.height() / 2 + i * lineSpacing + scrollY, Util.paintSmwGameFont);

                i++;
            }
        }
    }

    public void addGame( Game game ) {
        games.add(game);
    }

    public void handleKey( int keyCode, boolean down ) {
        if ( down ) {
            if (keyCode == Util.PTKEY_UP) {
                if (currentSelection > 0 ) {
                    currentSelection--;
                    viewConsole.home.soundPool.play(viewConsole.soundGameSelect, 1, 1, 1, 0, 1.0f);
                }
            } else if (keyCode == Util.PTKEY_DOWN) {
                if (currentSelection < games.size() - 1 ) {
                    currentSelection++;
                    viewConsole.home.soundPool.play(viewConsole.soundGameSelect, 1, 1, 1, 0, 1.0f);
                }
            }

            if (keyCode == Util.PTKEY_A || keyCode == Util.PTKEY_B) {
                if ( !games.get(currentSelection).name.startsWith("Dummy") ) {
                    viewConsole.home.launchGame(games.get(currentSelection));
                    viewConsole.home.soundPool.play(viewConsole.soundGameLaunch, 1, 1, 1, 0, 1.0f);
                }

//                selected = true;
            }
        }
    }
}
