package com.example.martin.mamehome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

/**
 * Created by Martin on 9/26/2015.
 */
public class Util {
    public final static int PTKEY_LEFT = 11;
    public final static int PTKEY_RIGHT = 8;
    public final static int PTKEY_UP = 10;
    public final static int PTKEY_DOWN = 9;
    public final static int PTKEY_A = 13;
    public final static int PTKEY_B = 42;
    public final static int PTKEY_X = 12;
    public final static int PTKEY_Y = 30 ;
    public final static int PTKEY_LEFT_SHOULDER = 29;
    public final static int PTKEY_RIGHT_SHOULDER = 66;
    public final static int PTKEY_START = 7;
    public final static int PTKEY_SELECT = 16;
    public final static int PTKEY_PAUSE = 15;
    public final static int PTKEY_EXIT = 14;
    public final static int PTKEY_VOL_DOWN = 32;
    public final static int PTKEY_VOL_UP = 31;

    public static Paint paintBitmap = null;
    public static Paint paintSmwFont = null;
    public static Paint paintSmwGameFont = null;
    public static Paint paintShape = null;

    public static void initialize(Context context) {
        paintBitmap = new Paint();
        paintBitmap.setARGB(255, 255, 255, 255);
        paintBitmap.setFilterBitmap(false);
        paintBitmap.setAntiAlias(false);
        paintBitmap.setDither(false);

        paintShape = new Paint();
        paintShape.setAntiAlias(false);
        paintShape.setDither(false);
        paintShape.setARGB(255, 255, 255, 255);

        Typeface smwTf = Typeface.createFromAsset(context.getAssets(), "fonts/Munro.ttf");
        paintSmwFont = new Paint();
        paintSmwFont.setTypeface(smwTf);
        paintSmwFont.setAntiAlias(false);
        paintSmwFont.setTextSize(8);
        paintSmwFont.setARGB(255, 255, 255, 255);

        paintSmwGameFont = new Paint();
        paintSmwGameFont.setTypeface(smwTf);
        paintSmwGameFont.setAntiAlias(false);
        paintSmwGameFont.setTextSize(8);
        paintSmwGameFont.setARGB(255, 255, 255, 255);
    }

    public static void drawBitmapFrame( Canvas canvas, Bitmap src, int frameWidth, int frameNum, int x, int y ) {

        int frameHeight = src.getHeight();
        canvas.drawBitmap(src, new Rect(frameNum * frameWidth, 0, frameNum * frameWidth + frameWidth, frameHeight), new Rect(x, y, x + frameWidth, y + frameHeight), paintBitmap);
    }
}
