package com.example.martin.mamehome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.View;

import java.util.Random;

/**
 * Created by Martin on 9/26/2015.
 */
public class ViewConsole extends ViewArcade {
    private final String LOG_TAG = "ARCADE_HOME";

    public Home home;

    private Bitmap bitmapConsoles;

    private int soundWoosh = 0;
    private int soundConsoleSelect = 0;
    private int soundConsoleEnter = 0;
    private int soundConsoleExit = 0;

    public int soundGameSelect = 0;
    public int soundGameLaunch = 0;

    private float scrollX = 0;
    private int consoleSpacing = 150;

    private int currentSelection = 1;
    private float selectionProgress = 0.0f;
    private boolean selected = false;

    Console[] consoles;

    public ViewConsole(Home home) {
        super( home );
        this.home = home;
        setupConsoles();
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    private void setupConsoles() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        bitmapConsoles = BitmapFactory.decodeResource(getResources(), R.drawable.arcades, options);
        soundWoosh = home.soundPool.load(appContext, R.raw.swosh, 1);
        soundConsoleEnter = home.soundPool.load(appContext, R.raw.swosh, 1);
        soundConsoleExit = home.soundPool.load(appContext, R.raw.swosh, 1);
        soundConsoleSelect = home.soundPool.load(appContext, R.raw.game_launch, 1);
        soundGameSelect = home.soundPool.load(appContext, R.raw.game_select, 1);
        soundGameLaunch = home.soundPool.load(appContext, R.raw.game_launch, 1);


        consoles = new Console[5];

        consoles[0] = new Console( bitmapConsoles, 0, this, "Arcade", "com.seleuco.mame4droid", "/storage/sdcard0/MAME4droid/roms/" );
        consoles[0].addGame( new Game( consoles[0], "Burger Time", "btime.zip"));
        consoles[0].addGame( new Game( consoles[0], "Bust-a-Move", "pbobblen.zip"));
        consoles[0].addGame( new Game( consoles[0], "Centipede", "centiped.zip"));
        consoles[0].addGame( new Game( consoles[0], "Donkey Kong", "dkong.zip"));
        consoles[0].addGame( new Game( consoles[0], "Donkey Kong 3", "dkong3.zip"));
        consoles[0].addGame( new Game( consoles[0], "Frogger", "frogger.zip"));
        consoles[0].addGame( new Game( consoles[0], "Galaga", "galaga.zip"));
        consoles[0].addGame( new Game( consoles[0], "Joust", "joust.zip"));
        consoles[0].addGame( new Game( consoles[0], "Pacman", "pacman.zip"));
        consoles[0].addGame( new Game( consoles[0], "Q*Bert", "qbert.zip"));
        consoles[0].addGame( new Game( consoles[0], "Raiden", "raiden.zip"));
        consoles[0].addGame( new Game( consoles[0], "Smash TV", "smashtv.zip"));
        consoles[0].addGame( new Game( consoles[0], "Space Invaders", "invaders.zip"));

        consoles[1] = new Console( bitmapConsoles, 1, this, "Nintendo", "com.explusalpha.NesEmu", "/storage/sdcard0/nes/roms/" );
        consoles[1].addGame(new Game(consoles[1], "AD&D Pool of Radiance", "AD&D Pool of Radiance (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Battletoads", "Battletoads (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Bionic Commando", "Bionic Commando (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Bubble Bobble", "Bubble Bobble  (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Castlevania", "Castlevania (U) (PRG 0).nes"));
        consoles[1].addGame(new Game(consoles[1], "Castlevania 2 - Simon's Quest", "Castlevania 2 - Simon's Quest  (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Castlevania 3 - Dracula's Curse", "Castlevania 3 - Dracula's Curse (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Contra", "Contra  (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Crystalis", "Crystalis (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Destiny of an Emperor", "Destiny of an Emperor (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Double Dragon", "Double Dragon (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Double Dragon 2 - The Revenge", "Double Dragon 2 - The Revenge (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Dragon Warrior", "Dragon Warrior (U) (PRG 0).nes"));
        consoles[1].addGame(new Game(consoles[1], "Dragon Warrior 2", "Dragon Warrior 2 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Dragon Warrior 3", "Dragon Warrior 3 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Dragon Warrior 4", "Dragon Warrior 4 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Excitebike", "Excitebike (E).nes"));
        consoles[1].addGame(new Game(consoles[1], "Faxanadu", "Faxanadu (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Final Fantasy", "Final Fantasy (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Ghosts 'N Goblins", "Ghosts'n Goblins (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Kirby's Adventure", "Kirby's Adventure (U) (PRG 1) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "The Legend of Zelda", "Legend of Zelda, The (U) (PRG 0).nes"));
        consoles[1].addGame(new Game(consoles[1], "Mario Bros", "Mario Bros  (JU).nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man", "Mega Man (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man 2", "Mega Man 2 (E) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man 3", "Mega Man 3 (U) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man 4", "Mega Man 4 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man 5", "Mega Man 5 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Mega Man 6", "Mega Man 6  (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Metal Gear", "Metal Gear  (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Metroid", "Metroid (E) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "Mike Tyson's Punch-Out!!", "Mike Tyson's Punch-Out!! (E) (PRG 1) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "Ninja Gaiden", "Ninja Gaiden (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Ninja Gaiden 2 - The Dark Sword of Chaos", "Ninja Gaiden 2 - The Dark Sword of Chaos (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Paperboy", "Paperboy (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "River City Ransom", "River City Ransom (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Startropics", "Startropics (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Startropics 2 - Zoda's Revenge", "Startropics 2 - Zoda's Revenge (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Super C", "Super C (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Super Mario Bros", "Super Mario Bros  (PRG 1) (JU).nes"));
        consoles[1].addGame(new Game(consoles[1], "Super Mario Bros", "Super Mario Bros 2 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Super Mario Bros 3", "Super Mario Bros 3 (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Tetris", "Tetris (U) [!].nes"));
        consoles[1].addGame(new Game(consoles[1], "Willow", "Willow (U).nes"));
        consoles[1].addGame(new Game(consoles[1], "Zelda 2 - The Adventure of Link", "Zelda 2 - The Adventure of Link (U).nes"));

        consoles[2] = new Console( bitmapConsoles, 2, this, "Super Nintendo", "com.explusalpha.Snes9x", "/storage/sdcard0/snes/roms/" );
        consoles[2].addGame(new Game(consoles[2], "ActRaiser", "ActRaiser (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "ActRaiser 2", "ActRaiser 2 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Alladin", "Aladdin (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Alien 3", "Alien 3 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Blackthorne", "Blackthorne (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Breath of Fire", "Breath of Fire (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Breath of Fire II", "Breath of Fire II (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Castlevania - Dracula X", "Castlevania - Dracula X (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Chrono Trigger", "Chrono Trigger (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Contra III - The Alien Wars", "Contra III - The Alien Wars (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Demon's Crest", "Demon's Crest (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Donkey Kong Country", "Donkey Kong Country (U) (V1.2) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Donkey Kong Country 2 - Diddy's Kong Quest", "Donkey Kong Country 2 - Diddy's Kong Quest (U) (V1.1) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Donkey Kong Country 3 - Dixie Kong's Double Trouble", "Donkey Kong Country 3 - Dixie Kong's Double Trouble (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Earthbound", "Earthbound (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Earthworm Jim", "Earthworm Jim (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Earthworm Jim 2", "Earthworm Jim 2 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Final Fantasy II", "Final Fantasy II (U) (V1.0) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Final Fantasy III", "Final Fantasy III (U) (V1.1) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Final Fantasy IV", "Final Fantasy IV (J) [T+Eng3.21_J2E].smc"));
        consoles[2].addGame(new Game(consoles[2], "Final Fantasy V", "Final Fantasy V (J) [T+Eng1.1_RPGe].smc"));
        consoles[2].addGame(new Game(consoles[2], "Final Fantasy VI", "Final Fantasy VI (J) [T+Eng1.2_RPGOne].smc"));
        consoles[2].addGame(new Game(consoles[2], "Fire Emblem - Mystery of the Emblem", "Fire Emblem - Monshou no Nazo (J) (V1.1) [T+Eng005_Blaxor].smc"));
        consoles[2].addGame(new Game(consoles[2], "Fire Emblem - Genealogy of the Holy War", "Fire Emblem - Seisen no Keifu (J) [T+Eng.25_J2E].smc"));
        consoles[2].addGame(new Game(consoles[2], "Fire Emblem - Thraki 776", "Fire Emblem - Thraki 776 (J) (V.ROM) [T+Eng.03_Luxifer].smc"));
        consoles[2].addGame(new Game(consoles[2], "Flashback", "Flashback (E) (M3) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Harvest Moon", "Harvest Moon (E) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Illusion of Gaia", "Illusion of Gaia (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Kirby Super Star", "Kirby Super Star (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Kirby's Dream Land 3", "Kirby's Dream Land 3 (U).smc"));
        consoles[2].addGame(new Game(consoles[2], "Legend of Zelda - A Link to the Past", "Legend of Zelda, The - A Link to the Past (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Lord of the Rings", "JRR Tolkien's The Lord of the Rings - Volume 1 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Lost Vikings", "Lost Vikings, The (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Lost Vikings II", "Lost Vikings II, The (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Lufia & The Fortress of Doom", "Lufia & The Fortress of Doom (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Lufia II - Rise of the Sinistrals", "Lufia II - Rise of the Sinistrals (E) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Mega Man X", "Mega Man X (U) (V1.1) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Mega Man X2", "Mega Man X 2 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Mega Man X3", "Mega Man X 3 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Ogre Battle - The March of the Black Queen", "Ogre Battle - The March of the Black Queen (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Secret of Evermore", "Secret of Evermore (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Secret of Mana", "Secret of Mana (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Shadowrun", "Shadowrun (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Soul Blazer", "Soul Blazer (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Star Ocean", "Star Ocean (J) [T+Eng1.0_DeJap].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Bomberman", "Super Bomberman (U).smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Bomberman 2", "Super Bomberman 2 (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Castlevania IV", "Super Castlevania IV (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Double Dragon", "Super Double Dragon (E) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Ghouls 'N Ghosts", "Super Ghouls 'N Ghosts (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Mario Kart", "Super Mario Kart (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Mario RPG - Legend of the Seven Stars", "Super Mario RPG - Legend of the Seven Stars (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Mario World", "Super Mario World (USA).zip"));
        consoles[2].addGame(new Game(consoles[2], "Super Mario World 2 - Yoshi's Island", "Super Mario World 2 - Yoshi's Island (U) (M3) (V1.0) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Metroid", "Super Metroid (JU) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Super Punch-Out!!", "Super Punch-Out!! (U) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Terranigma", "Terranigma (E) [!].smc"));
        consoles[2].addGame(new Game(consoles[2], "Uncharted Waters - New Horizons", "Uncharted Waters - New Horizons (U).smc"));

        consoles[3] = new Console( bitmapConsoles, 3, this, "Gameboy Advance", "com.explusalpha.GbaEmu", "/storage/sdcard0/gba/roms/" );
        consoles[3].addGame(new Game(consoles[3], "Advance Wars", "Advance Wars (U) (V1.1) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Advance Wars 2 - Black Hole Rising", "Advance Wars 2 - Black Hole Rising (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Astro Boy - Omega Factor", "Astro Boy - Omega Factor (U) (M6).zip"));
        consoles[3].addGame(new Game(consoles[3], "Boktai - The Sun is in Your Hand", "Boktai - The Sun is in Your Hand (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Boktai 2 - Solar Boy Django", "Boktai 2 - Solar Boy Django (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Breath of Fire", "Breath of Fire (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Breath of Fire II", "Breath of Fire II (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Castlevania - Aria of Sorrow", "Castlevania - Aria of Sorrow (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Castlevania - Circle of the Moon", "Castlevania - Circle of the Moon (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Castlevania - Harmony of Dissonance", "Castlevania - Harmony of Dissonance (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Drill Dozer", "Drill Dozer (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Final Fantasy I & II - Dawn of Souls", "Final Fantasy I & II - Dawn of Souls (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Final Fantasy IV Advance", "Final Fantasy IV Advance (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Final Fantasy V Advance", "Final Fantasy V Advance (U)(Independent).zip"));
        consoles[3].addGame(new Game(consoles[3], "Final Fantasy VI Advance", "Final Fantasy VI Advance (U)(Xenophobia).zip"));
        consoles[3].addGame(new Game(consoles[3], "Final Fantasy Tactics Advance", "Final Fantasy Tactics Advance (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Fire Emblem", "Fire Emblem (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Fire Emblem - The Sacred Stones", "Fire Emblem - The Sacred Stones (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Golden Sun", "Golden Sun (UE) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Golden Sun - The Lost Age", "Golden Sun - The Lost Age (UE) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Harvest Moon - Friends of Mineral Town", "Harvest Moon - Friends of Mineral Town (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Harvest Moon - More Friends of Mineral Town", "Harvest Moon - More Friends of Mineral Town (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Kingdom Hearts - Chain of Memories", "Kingdom Hearts - Chain of Memories (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Kirby & The Amazing Mirror", "Kirby & The Amazing Mirror (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Kirby - Nightmare in Dreamland", "Kirby - Nightmare in Dreamland (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Legend of Zelda - A Link To The Past & Four Swords", "Legend of Zelda, The - A Link To The Past Four Swords (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Legend of Zelda - The Minish Cap", "Legend of Zelda, The - The Minish Cap (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Lunar Legend", "Lunar Legend (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario & Luigi - Superstar Saga", "Mario & Luigi - Superstar Saga (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario Golf - Advance Tour", "Mario Golf - Advance Tour (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario Kart Super Circuit", "Mario Kart Super Circuit (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario Party Advance", "Mario Party Advance (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario Tennis Advance - Power Tour", "Mario Tennis Advance - Power Tour (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Mario vs Donkey Kong", "Mario vs. Donkey Kong (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman & Bass", "Megaman & Bass (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Battle Network", "Megaman Battle Network (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Battle Network 2", "Megaman Battle Network 2 (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Zero", "Megaman Zero (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Zero 2", "Megaman Zero 2 (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Zero 3", "Megaman Zero 3 (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Megaman Zero 4", "Megaman Zero 4 (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Metal Slug Advance", "Metal Slug Advance (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Metroid - Zero Mission", "Metroid - Zero Mission (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Metroid Fusion", "Metroid Fusion (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Mr. Driller 2", "Mr. Driller 2 (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Ninja Five-0", "Ninja Five-0 (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Pokemon - Emerald Version", "Pokemon - Emerald Version (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Pokemon - Fire Red Version", "Pokemon - Fire Red Version (U) (V1.1).zip"));
        consoles[3].addGame(new Game(consoles[3], "Pokemon - Leaf Green Version", "Pokemon - Leaf Green Version (U) (V1.1).zip"));
        consoles[3].addGame(new Game(consoles[3], "Pokemon - Ruby Version", "Pokemon - Ruby Version (U) (V1.1).zip"));
        consoles[3].addGame(new Game(consoles[3], "Pokemon - Sapphire Version", "Pokemon - Sapphire Version (U) (V1.1).zip"));
        consoles[3].addGame(new Game(consoles[3], "Sonic Advance", "Sonic Advance (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Sonic Advance 2", "Sonic Advance 2 (U) (M6) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Sonic Advance 3", "Sonic Advance 3 (U) (M6).zip"));
        consoles[3].addGame(new Game(consoles[3], "Super Mario Advance 2 - Super Mario World", "Super Mario Advance 2 - Super Mario World (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Super Mario Advance 3 - Yoshi's Island", "Super Mario Advance 3 - Yoshi's Island (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Super Mario Advance 4 - Super Mario Bros. 3", "Super Mario Advance 4 - Super Mario Bros. 3 (U) (V1.1).zip"));
        consoles[3].addGame(new Game(consoles[3], "Super Robot Taisen Original Generation", "Super Robot Taisen Original Generation (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Sword of Mana", "Sword of Mana (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Tactics Ogre - The Knight of Lodis", "Tactics Ogre - The Knight of Lodis (U) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "Tales of Phantasia", "Tales of Phantasia (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Wario Land - Super Mario Land 3", "Wario Land - Super Mario Land 3 (JUE) [!].gb"));
        consoles[3].addGame(new Game(consoles[3], "Wario Land 4", "Wario Land 4 (UE) [!].zip"));
        consoles[3].addGame(new Game(consoles[3], "WarioWare - Twisted!", "WarioWare - Twisted! (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "WarioWare Inc.", "WarioWare Inc. (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Yu-Gi-Oh! - GX Duel Academy", "Yu-Gi-Oh! - GX Duel Academy (U).zip"));
        consoles[3].addGame(new Game(consoles[3], "Yu-Gi-Oh! - The Sacred Cards", "Yu-Gi-Oh! - The Sacred Cards (U) [!].zip"));

        consoles[4] = new Console( bitmapConsoles, 4, this, "Sega Genesis", "com.explusalpha.MdEmu", "/storage/sdcard0/md/roms/" );
        consoles[4].addGame( new Game(consoles[4], "Adventures of Batman and Robin", "Adventures of Batman and Robin, The (JU) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Beyond Oasis", "Beyond Oasis (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Bubble and Squeak", "Bubble and Squeak (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Castlevania - Bloodlines", "Castlevania - Bloodlines (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Contra - Hard Corps", "Contra - Hard Corps (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Cool Spot", "Cool Spot (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Crusader of Centy", "Crusader of Centy (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "ECCO - The Tides of Time", "ECCO - The Tides of Time (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "ESWAT Cyber Police - City Under Siege", "ESWAT Cyber Police - City Under Siege (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Elemental Master", "Elemental Master (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "General Chaos", "General Chaos (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Ghouls 'N Ghosts", "Ghouls 'N Ghosts (W) (REV 02) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Gunstar Heroes", "Gunstar Heroes (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Haunting", "Haunting, The (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Herzog Zwei", "Herzog Zwei (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Landstalker - The Treasures of King Nole", "Landstalker - The Treasures of King Nole (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Light Crusader", "Light Crusader (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Mega Bomberman", "Mega Bomberman (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Mega Lo Mania", "Mega Lo Mania (E) (REV 00) [c][!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Mega Turrican", "Mega Turrican (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Phantasy Star - The End of the Millenium", "Phantasy Star - The End of the Millenium (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Phantasy Star II", "Phantasy Star II (UE) (REV 02) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Phantasy Star III - Generations of Doom", "Phantasy Star III - Generations of Doom (UE) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Phantasy Star IV", "Phantasy Star IV (USA).zip"));
        consoles[4].addGame( new Game(consoles[4], "Populous", "Populous (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Populous II - Two Tribes", "Populous II - Two Tribes (E) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Rolo to the Rescue", "Rolo to the Rescue (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shadow Dancer", "Shadow Dancer (W) [c][!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shadowrun", "Shadow Run (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shining Force", "Shining Force (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shining Force II", "Shining Force II (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shining in the Darkness", "Shining in the Darkness (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Shinobi 3 - Return of the Ninja Master", "Shinobi 3 - Return of the Ninja Master (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Sonic The Hedgehog", "Sonic The Hedgehog (W) (REV 00) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Sonic The Hedgehog 2", "Sonic The Hedgehog 2 (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Sonic The Hedgehog 3", "Sonic The Hedgehog 3 (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Starflight", "Starflight (U) (REV 01) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Strider", "Strider (UE) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Sword of Vermilion", "Sword of Vermilion (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Tiny Toon Adventures - Acme All-Stars", "Tiny Toon Adventures - Acme All-Stars (JU) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Tiny Toon Adventures - Buster's Hidden Treasure", "Tiny Toon Adventures - Buster's Hidden Treasure (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Toejam & Earl", "Toejam & Earl (U) (REV 00) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Toejam & Earl in Panic on Funkotron", "Toejam & Earl in Panic on Funkotron (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Warsong", "Warsong (U) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Wiz 'n' Liz", "Wiz 'n' Liz (W) [!].bin"));
        consoles[4].addGame( new Game(consoles[4], "Zombies Ate My Neighbors", "Zombies Ate My Neighbors (U) [c][!].bin"));
    }

    @Override
    protected void update( float timeDelta) {
        int destX = currentSelection * -consoleSpacing;

        float delta = (destX - scrollX) / 4 * timeDelta * 20.0f;
        if ( delta < 0 && delta > -1.0f ) delta = -1.0f;
        if ( delta > 0 && delta < 1.0f ) delta = 1.0f;


        scrollX += delta;
        if ( Math.abs(destX - scrollX) < 1.1f ) scrollX = destX;

        if ( selected ) {
            selectionProgress += ( 1.0f - selectionProgress ) * timeDelta * 4.0f;
            if (selectionProgress > 1) selectionProgress = 1;
        } else {
            selectionProgress += ( 0.0f - selectionProgress ) * timeDelta * 4.0f;
            if (selectionProgress < 0) selectionProgress = 0;
        }

         for ( int i = 0; i < consoles.length; i++ ) {
            consoles[i].update(timeDelta);
        }
    }

    @Override
    protected void drawScene( Canvas canvas ) {
        int xOff = 0;
        int selectionScroll = 0;

        selectionScroll = (int)(-100 * (selectionProgress));


        Util.paintShape.setARGB(255, 16, 16, 16);
        canvas.drawRect(canvas.getWidth() / 2 - 70 + selectionScroll, 0, canvas.getWidth() / 2 + 70 + selectionScroll, canvas.getHeight(), Util.paintShape);

        canvas.save();
        canvas.translate((int) scrollX, 0);
        for ( int i = 0; i < consoles.length; i++ ) {
            canvas.save();
            canvas.translate(xOff, 0);

            if ( i != currentSelection ) {
                Util.paintBitmap.setARGB((int) ((1 - selectionProgress) * 255), 255, 255, 255);
                Util.paintSmwFont.setARGB((int) ((1 - selectionProgress) * 255), 255, 255, 255);

                Util.paintSmwGameFont.setARGB((int) ((selectionProgress) * 255), 255, 255, 255);
            } else {
                canvas.translate( selectionScroll, 0 );
                if ( selected ) {
                    Util.paintSmwGameFont.setARGB((int) ((selectionProgress) * 255), 255, 255, 255);
                }
            }
            consoles[i].draw(canvas, i == currentSelection);
            Util.paintBitmap.setARGB(255, 255, 255, 255);
            Util.paintSmwFont.setARGB(255, 255, 255, 255);

            xOff += consoleSpacing;
            canvas.restore();
        }

        canvas.restore();
    }

    @Override
    public void handleKey( int keyCode, boolean down ) {
        if ( down ) {
            if ( !selected ) {
                if (keyCode == Util.PTKEY_LEFT || keyCode == Util.PTKEY_LEFT_SHOULDER) {
                    if (currentSelection > 0 ) {
                        currentSelection--;
                        home.soundPool.play(soundWoosh, 1, 1, 1, 0, 1.0f);
                    }
                } else if (keyCode == Util.PTKEY_RIGHT || keyCode == Util.PTKEY_RIGHT_SHOULDER) {
                    if (currentSelection < consoles.length - 1 ) {
                        currentSelection++;
                        home.soundPool.play(soundWoosh, 1, 1, 1, 0, 1.0f);
                    }
                }
            }

            if ( selected ) {
                consoles[currentSelection].handleKey(keyCode, down);
            } else {
                if (keyCode == Util.PTKEY_A || keyCode == Util.PTKEY_B) {
                    selected = true;
                    home.soundPool.play(soundConsoleEnter, 1, 1, 1, 0, 1.0f);
                }
            }

            if (keyCode == Util.PTKEY_X || keyCode == Util.PTKEY_Y) {
                selected = false;
                home.soundPool.play(soundConsoleExit, 1, 1, 1, 0, 1.0f);
            }

            if ( Home.keyStates[Util.PTKEY_LEFT_SHOULDER] && Home.keyStates[Util.PTKEY_RIGHT_SHOULDER] ) {
                if (keyCode == Util.PTKEY_START ) {
                    Game tempGame = new Game(consoles[currentSelection], "NOROM", "NOROM");
                    home.launchGame(tempGame);
                }
            }
        }
    }
}
