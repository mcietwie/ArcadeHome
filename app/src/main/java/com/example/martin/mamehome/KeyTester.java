package com.example.martin.mamehome;

import android.app.Activity;
import android.view.KeyEvent;

/**
 * Created by Martin on 9/3/2015.
 */
public class KeyTester {
    Home home;
    boolean[] keyStatus = new boolean[128];

    public KeyTester( Home activity ) {
        home = activity;
        //home.setConsoleText("");
    }

    public void updateKeymap( ) {
        String keymap = "";
        for ( int i = 0; i < 128; i++ ) {
            if ( keyStatus[i] ) {
                keymap += ""+i;
            } else {
                keymap += ".";
            }
            if ( i % 32 == 0 && i > 0 ) {
                keymap += "\n";
            }
        }

       // home.setConsoleText(keymap);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        keyStatus[keyCode] = true;
        updateKeymap();
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        keyStatus[keyCode] = false;
        updateKeymap();
        return true;
    }

}
