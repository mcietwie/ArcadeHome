package com.example.martin.mamehome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Martin on 9/26/2015.
 */
public class ViewArcade extends View {
    private final String LOG_TAG = "ARCADE_HOME";

    protected Context appContext;

    private Canvas canvasTarget;
    private Bitmap bitmapTarget;

    protected int screenWidth;
    protected int screenHeight;

    private long previousTimestamp = -1;

    public ViewArcade(Context context) {
        super(context);
        appContext = context;

        bitmapTarget = null;

        initialize();
    }

    protected void initialize() {
    }

    protected void update( float timeDelta) {
    }

    protected void drawScene( Canvas canvas ) {
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        float timeDelta = 0;
        long curTimestamp = System.currentTimeMillis();
        if ( previousTimestamp != -1 ) {
            timeDelta = (curTimestamp - previousTimestamp) / 1000.0f;
        }
        update( timeDelta );
        previousTimestamp = curTimestamp;

        super.onDraw(canvas);

        Util.paintShape.setARGB(255, 0, 0, 0);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), Util.paintShape);

        float pixelScale = 3;
        screenWidth = (int)(canvas.getWidth() / pixelScale);
        screenHeight = (int)(canvas.getHeight() / pixelScale);
        if (bitmapTarget == null) {
            bitmapTarget = Bitmap.createBitmap( screenWidth, screenHeight, Bitmap.Config.ARGB_8888 );
            bitmapTarget.setDensity( Bitmap.DENSITY_NONE );
            if ( bitmapTarget != null) canvasTarget = new Canvas( bitmapTarget );
            else return;
            if (canvasTarget == null ) return;
        }

        canvas.save();
        canvasTarget.save();
        bitmapTarget.eraseColor(0x00000000);

        drawScene(canvasTarget);

        canvasTarget.restore();

        canvas.scale(pixelScale, pixelScale);
        Util.paintBitmap.setARGB(255, 255, 255, 255);
        canvas.drawBitmap(bitmapTarget, 0, 0, Util.paintBitmap);
        canvas.restore();

        postInvalidateDelayed(16);
    }

    public void handleKey( int keyCode, boolean down ) {

    }

    public boolean handleTouch(MotionEvent event) {
        return true;
    }

}
