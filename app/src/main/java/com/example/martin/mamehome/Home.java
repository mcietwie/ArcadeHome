package com.example.martin.mamehome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Canvas;
import android.hardware.input.InputManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.InputEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.bluetooth.BluetoothAdapter;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class Home extends Activity {
    private final String LOG_TAG = "ARCADE_HOME";

    private KeyTester keyTester;

    private PackageManager manager;

    private SoundPool m_sp = null;
    private int m_volumeBleepSoundId = 0;

    ViewArcade currentView = null;

    ViewConsole viewConsole = null;
    ViewDebug viewDebug = null;

    public AudioManager audioManager = null;
    public SoundPool soundPool = null;


    public static boolean[] keyStates = new boolean[256];

    private static final String MAME_PACKAGE_NAME = "com.seleuco.mame4droid";
    private static final String ROMS_PATH = "/storage/sdcard0/MAME4droid/roms/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Util.initialize(this);

        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

        viewConsole = new ViewConsole( this );
        viewDebug = new ViewDebug( this );
        setView( viewConsole );

        keyTester = new KeyTester( this );

        manager = getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        m_sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        for ( int i = 0; i < keyStates.length; i++ ) {
            keyStates[i] = false;
        }
    }

    public void setView( ViewArcade view ) {
        setContentView( view );
        currentView = view;
    }

    public ViewArcade getView( ) {
        return currentView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent (MotionEvent event) {
        return getView().handleTouch(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /*
        if (!keyTester.onKeyDown(keyCode, event) ) {
            return super.onKeyDown(keyCode, event);
        }
*/
        keyStates[keyCode] = true;

        if ( Home.keyStates[Util.PTKEY_LEFT_SHOULDER] && Home.keyStates[Util.PTKEY_RIGHT_SHOULDER] ) {
            if (Home.keyStates[Util.PTKEY_A] || Home.keyStates[Util.PTKEY_B]) {
                if (getView() == viewConsole) setView(viewDebug);
                else if (getView() == viewDebug) setView(viewConsole);

                return true;
            }
        }

        getView().handleKey(keyCode, true);

        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        /*
        if ( keyCode == 48 || keyCode == 56 ) {
            return true;
        }

        if ( !keyTester.onKeyUp(keyCode, event) ) {
            return super.onKeyUp(keyCode, event);
        }
*/
        keyStates[keyCode] = false;
        getView().handleKey( keyCode, false );

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent sendIntent = new Intent();
        sendIntent.setAction("arcade.GAME_QUIT");
        //sendIntent.putExtra(Intent.EXTRA_TEXT, "Game Title");
        //sendIntent.setType("text/plain");
        sendBroadcast(sendIntent);
    }

    public boolean launchGame( Game game ) {
        String romName = game.console.romPath + game.romName;
        String emulatorName = game.console.emulatorName;

        Intent intent = manager.getLaunchIntentForPackage(emulatorName);

        if (intent == null) {
            Log.e(LOG_TAG, "null intent");
            return false;
        } else {
            if ( !romName.endsWith("NOROM") ) {
                File romFile = new File(romName);
                intent.setData(Uri.fromFile(romFile));
                intent.setAction(Intent.ACTION_VIEW);
            } else {
                Log.e(LOG_TAG, "No rom, default open");
                intent.setAction(Intent.ACTION_MAIN);
            }

            Log.e(LOG_TAG, "Sending intent: " + intent.toString());
            Home.this.startActivity(intent);
        }

        Intent sendIntent = new Intent();
        sendIntent.setAction("arcade.GAME_LAUNCHED");
        sendIntent.putExtra(Intent.EXTRA_TEXT, game.console.emulatorName);
        sendIntent.putExtra(Intent.EXTRA_TITLE, game.name);

        //sendIntent.setType("text/plain");
        sendBroadcast(sendIntent);

        return true;
    }
}
